##Run Instructions
1.  A Java 8 runtime is required to run the application
2.  Download the train.jar from the `train-code-test/jar` folder
3.  Run `java -jar train.jar` in terminal/cmd
4.  Open `http://localhost:8080/` in a browser
