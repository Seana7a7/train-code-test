package train;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

public class UnitTests {
	static Path graph;
	static Vertex a, b, c, d, e;

	// setup graph
	@BeforeClass
	public static void setUpBeforeClass() {
		graph = new Path();

		a = new Vertex("A");
		b = new Vertex("B");
		c = new Vertex("C");
		d = new Vertex("D");
		e = new Vertex("E");

		graph.pathTable.put(a, new Edge(a, b, 5).nextEdge(new Edge(a, d, 5)
				.nextEdge(new Edge(a, e, 7))));
		graph.pathTable.put(b, new Edge(b, c, 4));
		graph.pathTable.put(c, new Edge(c, d, 8).nextEdge(new Edge(c, e, 2)));
		graph.pathTable.put(d, new Edge(d, c, 8).nextEdge(new Edge(d, e, 6)));
		graph.pathTable.put(e, new Edge(e, b, 3));
	}

	// Q1
	@Test
	public void q1FindDistanceABC() {
		ArrayList<Vertex> route = new ArrayList<Vertex>();
		route.add(a);
		route.add(b);
		route.add(c);
		assertEquals(9, graph.findDistance(route));
	}
	
	// Q2
	@Test
	public void q2FindDistanceAD() {
		ArrayList<Vertex> route = new ArrayList<Vertex>();
		route.add(a);
		route.add(d);
		assertEquals(5, graph.findDistance(route));
	}
	
	// Q3
	@Test
	public void q3FindDistanceADC() {
		ArrayList<Vertex> route = new ArrayList<Vertex>();
		route.add(a);
		route.add(d);
		route.add(c);
		assertEquals(13, graph.findDistance(route));
	}

	// Q4
	@Test
	public void q4FindDistanceAEBCD() {
		ArrayList<Vertex> route = new ArrayList<Vertex>();
		route.add(a);
		route.add(e);
		route.add(b);
		route.add(c);
		route.add(d);
		assertEquals(22, graph.findDistance(route));
	}
	
	// Q5
	@Test
	public void q5FindDistanceBetweenAED() {
		ArrayList<Vertex> route = new ArrayList<Vertex>();
		route.add(a);
		route.add(e);
		route.add(d);
		assertEquals(-1, graph.findDistance(route));
	}

	// Q6
	@Test
	public void q6FindNumTripsCC3() {
		int numTrips = graph.numTrips(c, c, 3);
		assertEquals(2, numTrips);
	}

	// Q7
	@Test
	public void q7FindNumTripsAC4() {
		int numTrips = graph.numTripsMax(a, c, 4);
		assertEquals(3, numTrips);
	}

	// Q8
	@Test
	public void q8FindShortestRouteAC() {
		int shortestRoute = graph.shortestRoute(a, c);
		assertEquals(9, shortestRoute);
	}
	
	// Q9
	@Test
	public void q9FindShortestRouteBB() {
		int shortestRoute = graph.shortestRoute(b, b);
		assertEquals(9, shortestRoute);
	}

	// Q10
	@Test
	public void q10FindNumRoutesWithinCC30() throws Exception {
		int numRoutesWithin = graph.numRoutesWithin(c, c, 30);
		assertEquals(7, numRoutesWithin);
	}

}
