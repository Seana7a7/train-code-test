package train;

import static spark.Spark.get;
import static spark.SparkBase.port;
import static spark.SparkBase.staticFileLocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

public class App {

	public static void main(String[] args) {

		// web server config
		port(8080);
		staticFileLocation("/public");

		// graph setup
		Graph graph = new Graph();
		Path path = graph.getPath();

		// get root url
		get("/", (request, response) -> {
			Map<String, Object> model = new HashMap<>();
			model.put("title", "Greenfinch Code Test");

			return new ModelAndView(model, "/view/index.html");
		}, new VelocityTemplateEngine());

		get("/q1/", (request, response) -> {
			response.type("application/json");

			ArrayList<Vertex> vertices = new ArrayList<Vertex>();
			vertices.add(graph.getA());
			vertices.add(graph.getB());
			vertices.add(graph.getC());

			int result = path.findDistance(vertices);

			return result;
		});

		get("/q2/", (request, response) -> {
			response.type("application/json");

			ArrayList<Vertex> vertices = new ArrayList<Vertex>();
			vertices.add(graph.getA());
			vertices.add(graph.getD());

			int result = path.findDistance(vertices);

			return result;
		});

		get("/q3/", (request, response) -> {
			response.type("application/json");

			ArrayList<Vertex> vertices = new ArrayList<Vertex>();
			vertices.add(graph.getA());
			vertices.add(graph.getD());
			vertices.add(graph.getC());

			int result = path.findDistance(vertices);

			return result;
		});

		get("/q4/", (request, response) -> {
			response.type("application/json");

			ArrayList<Vertex> vertices = new ArrayList<Vertex>();
			vertices.add(graph.getA());
			vertices.add(graph.getE());
			vertices.add(graph.getB());
			vertices.add(graph.getC());
			vertices.add(graph.getD());

			int result = path.findDistance(vertices);

			return result;
		});

		get("/q5/", (request, response) -> {
			response.type("application/json");

			ArrayList<Vertex> vertices = new ArrayList<Vertex>();
			vertices.add(graph.getA());
			vertices.add(graph.getE());
			vertices.add(graph.getD());

			int result = path.findDistance(vertices);

			return result;
		});

		get("/q6/", (request, response) -> {
			response.type("application/json");

			int result = path.numTrips(graph.getC(), graph.getC(), 3);

			if (result > 0) {
				return result;
			} else {
				return "NO SUCH ROUTE";
			}
		});

		get("/q7/", (request, response) -> {
			response.type("application/json");

			int result = path.numTripsMax(graph.getA(), graph.getC(), 4);

			return result;
		});

		get("/q8/", (request, response) -> {
			response.type("application/json");

			int result = path.shortestRoute(graph.getA(), graph.getC());

			return result;
		});

		get("/q9/", (request, response) -> {
			response.type("application/json");

			int result = path.shortestRoute(graph.getB(), graph.getB());

			return result;
		});

		get("/q10/", (request, response) -> {
			response.type("application/json");

			int result = path.numRoutesWithin(graph.getC(), graph.getC(), 30);

			return result;
		});

	}

}
