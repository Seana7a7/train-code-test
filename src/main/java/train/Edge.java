package train;

/**
 * 
 * @author Sean
 *
 */
public class Edge {

	private Vertex originVertex;

	private Vertex destinationVertex;

	private int weight;

	private Edge nextEdge;

	public Edge(Vertex originVertex, Vertex destinationVertex, int weight) {
		this.originVertex = originVertex;
		this.destinationVertex = destinationVertex;
		this.weight = weight;
		this.nextEdge = null;
	}

	public Edge nextEdge(Edge edge) {
		this.nextEdge = edge;
		return this;
	}

	public Vertex getOriginVertex() {
		return originVertex;
	}

	public void setOriginVertex(Vertex originVertex) {
		this.originVertex = originVertex;
	}

	public Vertex getDestinationVertex() {
		return destinationVertex;
	}

	public void setDestinationVertex(Vertex destinationVertex) {
		this.destinationVertex = destinationVertex;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public Edge getNextEdge() {
		return nextEdge;
	}

	public void setNextEdge(Edge nextEdge) {
		this.nextEdge = nextEdge;
	}

}
