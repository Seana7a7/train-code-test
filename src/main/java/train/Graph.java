package train;

/**
 * 
 * @author Sean
 *
 */
public class Graph {

	private Path path;
	private Vertex a, b, c, d, e;

	// graph init
	public Graph() {

		path = new Path();

		a = new Vertex("A");
		b = new Vertex("B");
		c = new Vertex("C");
		d = new Vertex("D");
		e = new Vertex("E");

		path.pathTable.put(a, new Edge(a, b, 5).nextEdge(new Edge(a, d, 5)
				.nextEdge(new Edge(a, e, 7))));
		path.pathTable.put(b, new Edge(b, c, 4));
		path.pathTable.put(c, new Edge(c, d, 8).nextEdge(new Edge(c, e, 2)));
		path.pathTable.put(d, new Edge(d, c, 8).nextEdge(new Edge(d, e, 6)));
		path.pathTable.put(e, new Edge(e, b, 3));

	}

	public Path getPath() {
		return path;
	}

	public Vertex getA() {
		return a;
	}

	public Vertex getB() {
		return b;
	}

	public Vertex getC() {
		return c;
	}

	public Vertex getD() {
		return d;
	}

	public Vertex getE() {
		return e;
	}

}