package train;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * 
 * @author Sean
 *
 */
public class Path {
	public Hashtable<Vertex, Edge> pathTable;

	public Path() {
		this.pathTable = new Hashtable<Vertex, Edge>();
	}

	/*
	 * Q1-5
	 * 
	 * find edge weight between two or more vertices
	 */
	public int findDistance(ArrayList<Vertex> vertices) {

		// total weight count
		int totalWeight = 0;
		// vertices traversed
		int traversed = 0;

		int i = 0;

		// return 0 as not enough vertices given
		// to travel through graph
		if (vertices.size() < 2) {
			return 0;
		}

		while (i < vertices.size() - 1) {
			// if has a valid path to destination
			if (this.pathTable.containsKey(vertices.get(i))) {
				Edge edge = this.pathTable.get(vertices.get(i));

				while (edge != null) {
					// if the next vertex has a valid path to destination
					if (edge.getDestinationVertex().equals(vertices.get(i + 1))) {
						totalWeight += edge.getWeight();
						traversed++;
						break;
					}
					edge = edge.getNextEdge();
				}
			} else {
				// return -1 as no path available
				return -1;
			}
			i++;
		}

		// return -1 as no path available
		if (traversed != vertices.size() - 1) {
			return -1;
		}

		return totalWeight;

	}

	/*
	 * Q6
	 * 
	 * find the number of trips between two vertices given a maximum number of
	 * trips
	 * 
	 * wrapper class
	 */
	public int numTrips(Vertex startVertex, Vertex endVertex, int maxTrips) {
		return findPaths(startVertex, endVertex, 0, maxTrips);
	}

	/*
	 * Q6
	 * 
	 * find the number of trips between two vertices given a maximum number of
	 * trips
	 */
	private int findPaths(Vertex startVertex, Vertex endVertex, int traversed,
			int maxTrips) {

		// possible number of paths
		int paths = 0;

		// if the start and end vertices are connected
		if (this.pathTable.containsKey(startVertex)
				&& this.pathTable.containsKey(endVertex)) {

			traversed++;
			// not possible return 0
			if (traversed > maxTrips) {
				return 0;
			}
			startVertex.setVisited(true);
			Edge edge = this.pathTable.get(startVertex);
			while (edge != null) {

				// if vertices are connected move to next vertex
				if (edge.getDestinationVertex().equals(endVertex)) {
					paths++;
					edge = edge.getNextEdge();
					continue;
				}
				// if vertex has already been already visited move back
				else if (!edge.getDestinationVertex().isVisited()) {
					paths += findPaths(edge.getDestinationVertex(), endVertex,
							traversed, maxTrips);
					traversed--;
				}
				edge = edge.getNextEdge();
			}
		} else {
			// return -1 as no path possible
			return -1;
		}

		startVertex.setVisited(false);
		return paths;
	}

	/*
	 * Q7
	 * 
	 * find the number of trips between two vertices given a maximum number of
	 * trips
	 * 
	 * wrapper class
	 */
	public int numTripsMax(Vertex startVertex, Vertex endVertex, int trips) {
		return findPathsMax(startVertex, endVertex, 0, trips);
	}

	/*
	 * Q7
	 * 
	 * find the number of trips between two vertices given a maximum number of
	 * trips
	 */
	private int findPathsMax(Vertex startVertex, Vertex endVertex,
			int traversed, int trips) {

		// possible number of paths
		int paths = 0;

		// if the start and end vertices are connected
		if (this.pathTable.containsKey(startVertex)
				&& this.pathTable.containsKey(endVertex)) {

			traversed++;

			// not possible return 0
			if (traversed >= trips) {
				return 0;
			}

			startVertex.setVisited(true);
			Edge edge = this.pathTable.get(startVertex);
			while (edge != null) {
				// if vertices are connected move to next vertex
				if (edge.getDestinationVertex().equals(endVertex)) {
					paths++;
					edge = edge.getNextEdge();
					continue;
				}
				// if vertex has already been already visited move back
				else if (!edge.getDestinationVertex().isVisited()) {
					paths += findPathsMax(edge.getDestinationVertex(),
							endVertex, traversed, trips);
				}
				edge = edge.getNextEdge();
			}
		} else {
			// return -1 as no path possible
			return -1;
		}

		startVertex.setVisited(false);
		return paths;
	}

	/*
	 * Q8-9
	 * 
	 * Find the length of the shortest route between two vertices
	 * 
	 * wrapper class
	 */
	public int shortestRoute(Vertex startVertex, Vertex endVertex) {
		return findShortestRoute(startVertex, endVertex, 0, 0);
	}

	/*
	 * Q8-9
	 * 
	 * Find the length of the shortest route between two vertices
	 */
	private int findShortestRoute(Vertex startVertex, Vertex endVertex,
			int weight, int shortestPath) {

		// if the start and end vertices are connected
		if (this.pathTable.containsKey(startVertex)
				&& this.pathTable.containsKey(endVertex)) {

			startVertex.setVisited(true);
			Edge edge = this.pathTable.get(startVertex);
			while (edge != null) {

				if (edge.getDestinationVertex() == endVertex
						|| !edge.getDestinationVertex().isVisited()) {
					weight += edge.getWeight();
				}

				// if the start and end vertices are connected
				if (edge.getDestinationVertex().equals(endVertex)) {
					if (shortestPath == 0 || weight < shortestPath) {
						shortestPath = weight;
					}
					startVertex.setVisited(false);
					return shortestPath;
				}
				// if vertex has not been visited
				else if (!edge.getDestinationVertex().isVisited()) {
					shortestPath = findShortestRoute(
							edge.getDestinationVertex(), endVertex, weight,
							shortestPath);

					weight -= edge.getWeight();
				}
				edge = edge.getNextEdge();
			}
		} else {
			// return -1 as no path possible
			return -1;
		}

		startVertex.setVisited(false);
		return shortestPath;

	}

	/*
	 * Q10
	 * 
	 * Find the number of possible paths between two vertices
	 */
	public int numRoutesWithin(Vertex startVertex, Vertex endVertex,
			int maxWeight) {
		return findnumRoutesWithin(startVertex, endVertex, 0, maxWeight);
	}

	/*
	 * Q10
	 */
	private int findnumRoutesWithin(Vertex startVertex, Vertex endVertex,
			int weight, int maxWeight) {

		// possible number of paths
		int paths = 0;

		// if the start and end vertices are connected
		if (this.pathTable.containsKey(startVertex)
				&& this.pathTable.containsKey(endVertex)) {

			Edge edge = this.pathTable.get(startVertex);
			while (edge != null) {
				weight += edge.getWeight();

				if (weight <= maxWeight) {
					// if the start and end vertices are connected
					if (edge.getDestinationVertex().equals(endVertex)) {
						paths++;
						paths += findnumRoutesWithin(
								edge.getDestinationVertex(), endVertex, weight,
								maxWeight);
						edge = edge.getNextEdge();
						continue;
					} else {
						paths += findnumRoutesWithin(
								edge.getDestinationVertex(), endVertex, weight,
								maxWeight);
						weight -= edge.getWeight();

					}
				} else {
					weight -= edge.getWeight();
				}

				edge = edge.getNextEdge();
			}
		} else {
			// return -1 as no path possible
			return -1;
		}

		return paths;

	}

}
