$(document).ready(function() {

	$("#qButton").click(function() {

		$.getJSON("/q1/", function(data) {
			checkNoPath("#qOne", 1, data);
		});

		$.getJSON("/q2/", function(data) {
			checkNoPath("#qTwo", 2, data);
		});

		$.getJSON("/q3/", function(data) {
			checkNoPath("#qThree", 3, data);
		});

		$.getJSON("/q4/", function(data) {
			checkNoPath("#qFour", 4, data);
		});

		$.getJSON("/q5/", function(data) {
			checkNoPath("#qFive", 5, data);
		});

		$.getJSON("/q6/", function(data) {
			checkNoPath("#qSix", 6, data);
		});

		$.getJSON("/q7/", function(data) {
			checkNoPath("#qSeven", 7, data);
		});

		$.getJSON("/q8/", function(data) {
			checkNoPath("#qEight", 8, data);
		});

		$.getJSON("/q9/", function(data) {
			checkNoPath("#qNine", 9, data);
		});

		$.getJSON("/q10/", function(data) {
			checkNoPath("#qTen", 10, data);
		});

		function checkNoPath(id, num, data) {
			if (data == -1) {
				$(id).val("Output #" + num + ": NO SUCH ROUTE");
			} else {
				$(id).val("Output #" + num + ": " + data);
			}
		}

	});

});